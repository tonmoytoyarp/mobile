<?php
    session_start();
?>
<html>
    <head>
        <title>Mobile Models</title>
    </head>
    <body>
        <fieldset>
            <legend>Add your favorite mobile models here</legend>
            <form action='store.php' method="POST">
                <?php 
                    if (isset($_SESSION["Message"]) && !empty($_SESSION["Message"])){
                        echo $_SESSION["Message"];
                        unset($_SESSION["Message"]);
                    }
                    
                ?>
                <label>Enter the models name here:</label>
                <input type="text" name="Mobile Models">
                <input type="submit" value="Add">
            </form>
        </fieldset>
    </body>
</html>

